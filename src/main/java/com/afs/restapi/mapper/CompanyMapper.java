package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyMapper {
    public static CompanyResponse getCompanyResponse(Company company) {
        int employeeSize = company.getEmployees().size();
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeeSize(employeeSize);
        return companyResponse;
    }

    public static Company toEntity(CompanyCreateRequest companyCreateRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyCreateRequest, company);
        return company;
    }

    public static List<CompanyResponse> getCompanyResponses(List<Company> companiesFound) {
        return companiesFound.stream().map(CompanyMapper::getCompanyResponse).collect(Collectors.toList());
    }
}