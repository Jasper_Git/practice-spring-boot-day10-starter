package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.CompanyUpdateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<CompanyResponse> getAll() {
        List<Company> companiesFound = companyRepository.findAll();
        return CompanyMapper.getCompanyResponses(companiesFound);
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        List<Company> companies = companyRepository.findAll(PageRequest.of(page, pageSize)).toList();
        return CompanyMapper.getCompanyResponses(companies);
    }

    public CompanyResponse findById(Integer companyId) {
        Company companyFound = findCompanyById(companyId);
        return CompanyMapper.getCompanyResponse(companyFound);
    }

    private Company findCompanyById(Integer companyId) {
        return companyRepository.findById(companyId).
                orElseThrow(CompanyNotFoundException::new);
    }

    public CompanyResponse create(CompanyCreateRequest companyCreateRequest) {
        Company companySaved = companyRepository.save(CompanyMapper.toEntity(companyCreateRequest));
        return CompanyMapper.getCompanyResponse(companySaved);
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyUpdateRequest companyUpdateRequest) {
        Company company = findCompanyById(companyId);
        if (companyUpdateRequest.getName() != null) {
            company.setName(companyUpdateRequest.getName());
        }
        return CompanyMapper.getCompanyResponse(companyRepository.save(company));
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        List<Employee> employeesFound = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees();
        return EmployeeMapper.getEmployeeResponses(employeesFound);
    }
}
