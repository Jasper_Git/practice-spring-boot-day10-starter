package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        List<Employee> employeesFound = employeeRepository.findAllByStatusTrue();
        return EmployeeMapper.getEmployeeResponses(employeesFound);
    }

    public EmployeeResponse update(int id, EmployeeUpdateRequest employeeUpdateRequest) {
        Employee employeeFound = findActiveEmployee(id);
        Employee employeeUpdated = updateAgeAndStatus(employeeFound, employeeUpdateRequest);
        return EmployeeMapper.getEmployeeResponse(employeeUpdated);
    }

    public EmployeeResponse findById(int id) {
        Employee employeeFound = findActiveEmployee(id);
        return EmployeeMapper.getEmployeeResponse(employeeFound);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        List<Employee> employeesFound = employeeRepository.findByGenderAndStatusTrue(gender);
        return EmployeeMapper.getEmployeeResponses(employeesFound);
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        List<Employee> employees = employeeRepository.findAllByStatusTrue(PageRequest.of(pageNumber, pageSize)).toList();
        return EmployeeMapper.getEmployeeResponses(employees);
    }

    public EmployeeResponse insert(EmployeeCreateRequest employeeCreateRequest) {
        Employee employeeSaved = employeeRepository.save(EmployeeMapper.toEntity(employeeCreateRequest));
        return EmployeeMapper.getEmployeeResponse(employeeSaved);
    }

    public void delete(int id) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isPresent()) {
            employeeOptional.get().setStatus(false);
            employeeRepository.save(employeeOptional.get());
        } else {
            throw new EmployeeNotFoundException();
        }
    }

    public Employee updateAgeAndStatus(Employee toUpdateEmployee, EmployeeUpdateRequest employeeUpdateRequest) {
        if (employeeUpdateRequest.getAge() != null) {
            toUpdateEmployee.setAge(employeeUpdateRequest.getAge());
        }
        if (employeeUpdateRequest.getSalary() != null) {
            toUpdateEmployee.setSalary(employeeUpdateRequest.getSalary());
        }
        employeeRepository.save(toUpdateEmployee);
        return toUpdateEmployee;
    }

    private Employee findActiveEmployee(int id) {
        return employeeRepository.findByIdAndStatusTrue(id).
                orElseThrow(EmployeeNotFoundException::new);
    }
}
