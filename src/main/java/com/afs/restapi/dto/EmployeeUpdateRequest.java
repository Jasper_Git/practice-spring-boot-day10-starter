package com.afs.restapi.dto;

/**
 * @program: practice-spring-boot-day10-starter
 * @author: yoki
 * @create: 2023-07-22 12:43
 */
public class EmployeeUpdateRequest {
    private Integer age;
    private Integer salary;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
