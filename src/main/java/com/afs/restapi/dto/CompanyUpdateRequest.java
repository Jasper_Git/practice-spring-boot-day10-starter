package com.afs.restapi.dto;

/**
 * @program: practice-spring-boot-day10-starter
 * @author: yoki
 * @create: 2023-07-22 11:53
 */
public class CompanyUpdateRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
