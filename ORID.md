## Objective

- Today, I focused on using Data Transfer Object to replace entity objects when the client interacts with the server, and three groups gave talks on microservices, containers, and CI/CD.

## Reflective

- In the process of completing the teamwork of the group and participating in the presentations of other groups, being able to preview some cloud-native content in advance is called "planning ahead".

## Interpretive

- Correlation between microservices, Docker container, and CI/CD:
  - Each microservice can be packaged as an independent container with its own runtime environment and dependencies, enabling independent deployment of microservices.
  - CI/CD pipelines can integrate the build, test, delivery, and deployment processes of multiple microservices, ensure the quality and stability of microservices, and quickly push changes to the production environment.
  - By using Docker images to standardize the environment in which applications are built and run, you can reduce environment configuration issues and improve the efficiency and reliability of continuous integration and continuous delivery.
- DTO can simplify or filter entity objects, expose only the attributes that need to be transmitted, hide sensitive information or complex associations of entity objects, and protect the security and privacy of data.

## Decisional

- After the afternoon Retro, realizing the problem that the team could not provide feedback to the team members' task results in time, the team should plan the teamwork communication time in advance.

